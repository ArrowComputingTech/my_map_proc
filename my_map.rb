#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def my_map(*prc)
    retr = []
    if prc.count == 0
      self.each do |x|
        retr << yield(x)
      end
    else
      proc = prc[0]
      retr = self.each(&proc)
    end
    retr
  end
end

arr = [3,4,5,6,7]
puts "First round--"
mult2 = Proc.new { |x| x * 2 }
puts arr.my_map(mult2)
puts "Second round--"
puts arr.my_map { |x| x * 3 }
